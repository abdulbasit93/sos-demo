import 'react-native-gesture-handler';
import React from 'react';

import {useColorScheme} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {DefaultTheme, DarkTheme} from '@react-navigation/native';

import VehicleInfoScreen from './screens/VehicleInfoScreen';
import HomeScreen from './screens/HomeScreen';
import ServiceHistoryScreen from './screens/ServiceHistoryScreen';
import GalleryScreen from './screens/GalleryScreen';
import DocumentsScreen from './screens/DocumentsScreen';
import TestScreen from './screens/TestScreen';
import DrawerNavigationDemo from './screens/DrawerNavigationDemo';

import ImagePickerDemo from './screens/ImagePickerDemo';
import {MainStackNavigator} from './navigation/StackNavigator';
import DrawerNavigator from './navigation/DrawerNavigator';
import BottomTabNavigator from './navigation/TabNavigator';

const Stack = createNativeStackNavigator();

const App = () => {
  const scheme = useColorScheme();
  return (
    <NavigationContainer theme={scheme === 'dark' ? DarkTheme : DefaultTheme}>
      <DrawerNavigator />
    </NavigationContainer>
  );
};

export default App;

{
  /* <Stack.Navigator screenOptions={{headerShown: false}}>
  <Stack.Screen name="VehicleInfoScreen" component={Tabs} />

  <Stack.Screen
    name="GalleryScreen"
    component={GalleryScreen}
    options={{
      headerShown: true,
      headerTitle: 'Gallery',
      headerTintColor: '#fff',
      headerStyle: {backgroundColor: '#01ADEF'},
    }}
  />
  <Stack.Screen
    name="DocumentsScreen"
    component={DocumentsScreen}
    options={{
      headerShown: true,
      headerTitle: 'Documents',
      headerTintColor: '#fff',
      headerStyle: {backgroundColor: '#01ADEF'},
    }}
  />
</Stack.Navigator> */
}
