import React from 'react';

import {createDrawerNavigator} from '@react-navigation/drawer';

import {VehicleStackNavigator} from './StackNavigator';
import TabNavigator from './TabNavigator';
import GalleryScreen from '../screens/GalleryScreen';
import DocumentsScreen from '../screens/DocumentsScreen';

import {DrawerContent} from '../screens/DrawerContent';
import VehicleInfoScreen from '../screens/VehicleInfoScreen';

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator
      screenOptions={{
        headerShown: false,
        drawerPosition: 'right',
      }}
      drawerContent={props => <DrawerContent {...props} />}>
      <Drawer.Screen name="VehicleInfo" component={TabNavigator} />
      <Drawer.Screen name="GalleryScreen" component={GalleryScreen} />
      <Drawer.Screen name="DocumentsScreen" component={DocumentsScreen} />
      <Drawer.Screen name="VehicleInfoScreen" component={VehicleInfoScreen} />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;
