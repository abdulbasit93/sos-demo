import React from 'react';

import HomeScreen from '../screens/HomeScreen';
import MessagesScreen from '../screens/MessagesScreen';

import {createNativeStackNavigator} from '@react-navigation/native-stack';
import VehicleInfoScreen from '../screens/VehicleInfoScreen';

const Stack = createNativeStackNavigator();

const MainStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: 'green',
        },
        headerTintColor: 'white',
        headerBackTitle: 'Back',
      }}>
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
      <Stack.Screen name="Messages" component={MessagesScreen} />
    </Stack.Navigator>
  );
};

const VehicleStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="VehicleInfoScreen" component={VehicleInfoScreen} />
    </Stack.Navigator>
  );
};

export {MainStackNavigator, VehicleStackNavigator};
