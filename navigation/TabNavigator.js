import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import HomeScreen from '../screens/HomeScreen';
import VehicleInfoScreen from '../screens/VehicleInfoScreen';
import WalletScreen from '../screens/WalletScreen';
import ActiveServicesScreen from '../screens/ActiveServicesScreen';
import NotificationsScreen from '../screens/NotificationsScreen';

import FontistoIcon from 'react-native-vector-icons/Fontisto';
import IonIcon from 'react-native-vector-icons/Ionicons';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';

const Tab = createBottomTabNavigator();

const CustomTabBarButton = ({children, onPress}) => {
  return (
    <TouchableOpacity
      style={{
        top: -35,
        justifyContent: 'center',
        alignItems: 'center',
      }}
      onPress={onPress}>
      <View
        style={{
          paddingTop: 10,
          width: 80,
          height: 80,
          borderRadius: 40,
          backgroundColor: '#00AEF0',
        }}>
        {children}
      </View>
    </TouchableOpacity>
  );
};

const BottomTabNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName="VehicleInfoScreen"
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          backgroundColor: '#252525',
          height: 85,
          borderTopLeftRadius: 10,
          borderTopRightRadius: 10,
        },
        tabBarLabelStyle: {
          fontSize: 11,
        },
      }}>
      <Tab.Screen
        name="Vehicle Info"
        component={VehicleInfoScreen}
        options={{
          tabBarIcon: ({focused, color, size}) => {
            let iconName = 'car';
            return (
              <View>
                <IonIcon name={iconName} size={30} color={color} />
              </View>
            );
          },
          tabBarActiveTintColor: '#00AEEE',
          tabBarInactiveTintColor: '#fff',
        }}
      />
      <Tab.Screen
        name="Wallet"
        component={WalletScreen}
        options={{
          tabBarIcon: ({focused, color, size}) => {
            let iconName = 'wallet-outline';
            return (
              <View>
                <IonIcon name={iconName} size={30} color={color} />
              </View>
            );
          },
          tabBarActiveTintColor: '#00AEEE',
          tabBarInactiveTintColor: '#fff',
        }}
      />
      <Tab.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{
          tabBarIcon: ({focused, color, size}) => {
            let iconName = 'home';
            return (
              <View>
                <FontistoIcon name={iconName} size={30} color={color} />
              </View>
            );
          },
          tabBarLabel: '',
          tabBarActiveTintColor: '#252525',
          tabBarInactiveTintColor: '#fff',
          tabBarButton: props => <CustomTabBarButton {...props} />,
        }}
      />
      <Tab.Screen
        name="Active Services"
        component={ActiveServicesScreen}
        options={{
          tabBarIcon: ({focused, color, size}) => {
            let iconName = 'gears';
            return (
              <View>
                <FontAwesomeIcon name={iconName} size={30} color={color} />
              </View>
            );
          },
          tabBarActiveTintColor: '#00AEEE',
          tabBarInactiveTintColor: '#fff',
        }}
      />
      <Tab.Screen
        name="Notifications"
        component={NotificationsScreen}
        options={{
          tabBarIcon: ({focused, color, size}) => {
            let iconName = 'bell';
            return (
              <View>
                <FontistoIcon name={iconName} size={30} color={color} />
              </View>
            );
          },
          tabBarActiveTintColor: '#00AEEE',
          tabBarInactiveTintColor: '#fff',
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomTabNavigator;

//  <Tab.Navigator screenOptions={{headerShown: false}}>
//       <Tab.Screen
//         options={{headerShown: false}}
//         name="VehicleInfo"
//         component={VehicleStackNavigator}
//       />
//       <Tab.Screen name="HomeScreen" component={HomeScreen} />
//     </Tab.Navigator>
