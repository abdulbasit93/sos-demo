import React from 'react';
import {
  ScrollView,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

import Header from '../components/Header';
import DocumentsCard from '../components/DocumentsCard';
import GalleryCard from '../components/GalleryCard';
import InsuranceDetailCard from '../components/InsuranceDetailCard';
import VehicleInfoCard from '../components/VehicleInfoCard';
import BottomNavBar from '../components/BottomNavBar';

const VehicleInfoScreen = () => {
  return (
    <ScrollView style={styles.container}>
      {/* Header section */}
      <Header />

      {/* Vehicle Info Section */}

      <VehicleInfoCard />

      {/* Gallery Section  */}
      <GalleryCard />

      {/* Documents Section */}
      <DocumentsCard />

      {/* Insurance Detail Section */}
      <InsuranceDetailCard />

      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          margin: 30,
          marginBottom: 75,
        }}>
        <TouchableOpacity
          onPress={() => {}}
          style={{
            backgroundColor: '#01ADEF',
            padding: 20,
            width: 340,
            borderRadius: 50,
          }}>
          <Text
            style={{
              color: '#fff',
              fontSize: 20,
              fontWeight: '500',
              textAlign: 'center',
            }}>
            View Service History
          </Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default VehicleInfoScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#181818',
  },
});
