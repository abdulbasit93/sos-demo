import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
} from 'react-native';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import VehicleInfoScreen from './VehicleInfoScreen';
import MessagesScreen from './MessagesScreen';
import WalletScreen from './WalletScreen';
import ActiveServicesScreen from './ActiveServicesScreen';
import NotificationsScreen from './NotificationsScreen';
import HomeScreen from './HomeScreen';
import ServiceHistoryScreen from './ServiceHistoryScreen';

const Tab = createBottomTabNavigator();

const TestScreen = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarLabelStyle: {
          fontSize: 13,
          color: '#fff',
        },
        tabBarStyle: {
          backgroundColor: '#252525',
        },
      })}
      initialRouteName="VehicleInfoScreen">
      <Tab.Screen
        name="MessagesScreen"
        component={MessagesScreen}
        options={{headerShown: false, title: 'Messages'}}
      />
      <Tab.Screen
        name="WalletScreen"
        component={WalletScreen}
        options={{headerShown: false, title: 'Wallet'}}
      />
      <Tab.Screen
        name="ActiveServicesScreen"
        component={ActiveServicesScreen}
        options={{headerShown: false, title: 'Active Services'}}
      />
      <Tab.Screen
        name="NotificationsScreen"
        component={NotificationsScreen}
        options={{headerShown: false, title: 'Notifications'}}
      />
      <Tab.Screen
        name="VehicleInfoScreen"
        component={VehicleInfoScreen}
        options={{headerShown: false}}
      />
    </Tab.Navigator>
  );
};

export default TestScreen;
