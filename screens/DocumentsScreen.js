import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome5';

const DocumentsScreen = ({navigation, route: {params}}) => {
  // console.log('params ------> ', params.key);
  const uploadedDocName = params.key;
  return (
    <ScrollView style={styles.container}>
      <View
        style={{flexDirection: 'row', backgroundColor: '#000000', padding: 20}}>
        <TouchableOpacity
          style={{marginRight: 58, marginTop: 3}}
          onPress={() => navigation.goBack()}>
          <Icon name="chevron-left" size={23} color="#00AEEE" />
        </TouchableOpacity>
        <Text style={{fontSize: 22}}>Audi A4 Documents</Text>
        <TouchableOpacity
          style={{marginLeft: 48, marginTop: 4}}
          onPress={() => navigation.openDrawer()}>
          <Icon name="bars" size={23} color="#00AEEE" />
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: 'row', marginBottom: 20}}>
        {uploadedDocName ? (
          <View
            style={{
              backgroundColor: '#AB140C',
              width: 170,
              height: 170,
              borderRadius: 0,
              marginLeft: 10,
              marginRight: 5,
              padding: 10,
            }}>
            <Text style={{textAlign: 'center'}}>
              Document Uploaded: {uploadedDocName}
            </Text>
          </View>
        ) : (
          <Image
            source={require('../assets/images/dummy_PDF_doc.png')}
            style={{
              width: 170,
              height: 170,
              borderRadius: 5,
              marginLeft: 10,
              marginRight: 10,
            }}
          />
        )}
        <Image
          source={require('../assets/images/dummy_XL_doc.png')}
          style={{width: 170, height: 170, marginLeft: 15, marginRight: 10}}
          resizeMode="cover"
        />
      </View>

      <View style={{flexDirection: 'row', marginBottom: 20}}>
        <Image
          source={require('../assets/images/dummy_PDF_doc.png')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
        <Image
          source={require('../assets/images/dummy_XL_doc.png')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
      </View>

      <View style={{flexDirection: 'row', marginBottom: 20}}>
        <Image
          source={require('../assets/images/dummy_PDF_doc.png')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
        <Image
          source={require('../assets/images/dummy_XL_doc.png')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
      </View>

      <View style={{flexDirection: 'row', marginBottom: 20}}>
        <Image
          source={require('../assets/images/dummy_PDF_doc.png')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
        <Image
          source={require('../assets/images/dummy_XL_doc.png')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
      </View>

      <View style={{flexDirection: 'row', marginBottom: 20}}>
        <Image
          source={require('../assets/images/dummy_PDF_doc.png')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
        <Image
          source={require('../assets/images/dummy_XL_doc.png')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
      </View>

      <View style={{flexDirection: 'row', marginBottom: 20}}>
        <Image
          source={require('../assets/images/dummy_PDF_doc.png')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
        <Image
          source={require('../assets/images/dummy_XL_doc.png')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
      </View>
    </ScrollView>
  );
};

export default DocumentsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    padding: 10,
  },
});
