import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

import {createDrawerNavigator} from '@react-navigation/drawer';

import VehicleInfoScreen from './VehicleInfoScreen';

import {DrawerContent} from './DrawerContent';

const Drawer = createDrawerNavigator();

const DrawerNavigationDemo = () => {
  return (
    <Drawer.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: 'blue',
        },
        drawerPosition: 'right',
      }}
      drawerContent={props => <DrawerContent {...props} />}>
      <Drawer.Screen name="VehicleInfoScreen" component={VehicleInfoScreen} />
    </Drawer.Navigator>
  );
};

export default DrawerNavigationDemo;

const styles = StyleSheet.create({});
