import React from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome5';

import BottomTabNavigator from '../navigation/TabNavigator';
import placeholderImage from '../assets/images/image-placeholder-icon-5.jpg';

const GalleryScreen = ({navigation, route: {params}}) => {
  // console.log('params --> ', params.key.uri);
  const sourceimage = {uri: params.key.uri};
  return (
    <ScrollView style={styles.container}>
      <View
        style={{flexDirection: 'row', backgroundColor: '#000000', padding: 20}}>
        <TouchableOpacity
          style={{marginRight: 82, marginTop: 3}}
          onPress={() => navigation.goBack()}>
          <Icon name="chevron-left" size={23} color="#00AEEE" />
        </TouchableOpacity>
        <Text style={{fontSize: 22}}>Audi A4 Gallery</Text>
        <TouchableOpacity
          style={{marginLeft: 62, marginTop: 4}}
          onPress={() => navigation.openDrawer()}>
          <Icon name="bars" size={23} color="#00AEEE" />
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: 'row', marginBottom: 20}}>
        <Image
          source={sourceimage.uri ? sourceimage : placeholderImage}
          style={{
            width: 170,
            height: 170,
            borderRadius: 5,
            marginLeft: 10,
            marginRight: 10,
          }}
        />
        <Image
          source={require('../assets/images/601502.jpg')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
      </View>
      <View style={{flexDirection: 'row', marginBottom: 20}}>
        <Image
          source={require('../assets/images/601502.jpg')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
        <Image
          source={require('../assets/images/601502.jpg')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
      </View>
      <View style={{flexDirection: 'row', marginBottom: 20}}>
        <Image
          source={require('../assets/images/601502.jpg')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
        <Image
          source={require('../assets/images/601502.jpg')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
      </View>
      <View style={{flexDirection: 'row', marginBottom: 20}}>
        <Image
          source={require('../assets/images/601502.jpg')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
        <Image
          source={require('../assets/images/601502.jpg')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
      </View>
      <View style={{flexDirection: 'row', marginBottom: 20}}>
        <Image
          source={require('../assets/images/601502.jpg')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
        <Image
          source={require('../assets/images/601502.jpg')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
      </View>
      <View style={{flexDirection: 'row', marginBottom: 20}}>
        <Image
          source={require('../assets/images/601502.jpg')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
        <Image
          source={require('../assets/images/601502.jpg')}
          style={{width: 170, height: 170, marginLeft: 10, marginRight: 10}}
          resizeMode="cover"
        />
      </View>
    </ScrollView>
  );
};

export default GalleryScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    padding: 10,
  },
});
