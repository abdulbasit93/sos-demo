import React from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';

const ServiceHistoryScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Text style={{fontSize: 20, marginBottom: 30}}>
        Service History Screen
      </Text>
      <Button
        title="Go Back"
        onPress={() => navigation.navigate('VehicleInfoScreen')}
      />
    </View>
  );
};

export default ServiceHistoryScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#000',
  },
});
