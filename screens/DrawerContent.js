import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';

export function DrawerContent(props) {
  return (
    <View style={{flex: 1, margin: 20, marginTop: 30}}>
      <DrawerContentScrollView {...props}>
        <View style={styles.userContainer}>
          <View>
            <Image
              style={{width: 40, height: 40}}
              source={require('../assets/images/avatar.png')}
            />
          </View>

          <View>
            <Text style={{fontSize: 18}}>Richard Doe</Text>
            <Image
              style={{width: 90, height: 18}}
              source={require('../assets/images/five-stars.png')}
            />
          </View>
        </View>
        <View style={{marginTop: 30}}>
          <DrawerItem
            icon={({color, size}) => (
              <Icon name="account" color={color} size={size} />
            )}
            label="My Profile"
            onPress={() => {}}
          />
          <DrawerItem
            icon={({color, size}) => (
              <Icon name="car" color={color} size={size} />
            )}
            label="My Vehicles"
            onPress={() => {}}
          />
          <DrawerItem
            icon={({color, size}) => (
              <Icon name="file-document-outline" color={color} size={size} />
            )}
            label="Service Requests"
            onPress={() => {}}
          />
          <DrawerItem
            icon={({color, size}) => (
              <Icon name="bell" color={color} size={size} />
            )}
            label="Notifications"
            onPress={() => {}}
          />
          <DrawerItem
            icon={({color, size}) => (
              <Icon name="wallet-outline" color={color} size={size} />
            )}
            label="Wallet"
            onPress={() => {}}
          />
          <DrawerItem
            icon={({color, size}) => (
              <Icon name="message-text-outline" color={color} size={size} />
            )}
            label="Messages"
            onPress={() => {}}
          />
          <DrawerItem
            icon={({color, size}) => (
              <FontAwesomeIcon name="gears" color={color} size={size} />
            )}
            label="Active Services"
            onPress={() => {}}
          />
          <DrawerItem
            icon={({color, size}) => (
              <FontAwesomeIcon name="envelope-o" color={color} size={size} />
            )}
            label="Contact us"
            onPress={() => {}}
          />
          <DrawerItem
            icon={({color, size}) => (
              <Icon name="file-document" color={color} size={size} />
            )}
            label="Privacy Policy"
            onPress={() => {}}
          />
          <DrawerItem
            icon={({color, size}) => (
              <Icon name="exit-to-app" color={color} size={size} />
            )}
            label="Sign Out"
            onPress={() => {}}
          />
        </View>
      </DrawerContentScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  userContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
});
