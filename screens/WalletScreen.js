import React from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';

const WalletScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Text style={{fontSize: 22, marginBottom: 30}}>In Progress</Text>
    </View>
  );
};

export default WalletScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#000',
  },
});
