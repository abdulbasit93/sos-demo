import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';

import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

import placeholderImage from '../assets/images/image-placeholder-icon-5.jpg';

const ImagePickerDemo = () => {
  const [imageUri, setImageUri] = useState('');

  const openLibrary = () => {
    const options = {
      storageOptions: {
        path: 'images',
        mediaType: 'photo',
      },
    };

    launchImageLibrary(options, response => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // You can also display the image using data:
        const source = {
          uri: response.assets[0].uri,
        };
        setImageUri(source);
      }
    });
  };

  const openCamera = () => {
    const options = {
      storageOptions: {
        path: 'images',
        mediaType: 'photo',
      },
    };

    launchCamera(options, response => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // You can also display the image using data:
        const source = {
          uri: response.assets[0].uri,
        };
        setImageUri(source);
      }
    });
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.buttonStyle}
        onPress={() => openLibrary()}>
        <Text style={styles.buttonText}>Select from Library</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.buttonStyle} onPress={() => openCamera()}>
        <Text style={styles.buttonText}>Open Camera</Text>
      </TouchableOpacity>

      {/* {imageUri ? (
        <Image
          source={imageUri}
          style={{
            height: 280,
            width: 280,
            borderWidth: 2,
            borderColor: 'black',
            marginTop: 20,
          }}
        />
      ) : (
        <Image
          source={}
          style={{
            height: 280,
            width: 280,
            borderWidth: 2,
            borderColor: 'black',
            marginTop: 20,
          }}
        />
      )} */}

      <Image
        source={imageUri ? imageUri : placeholderImage}
        style={{
          height: 350,
          width: 350,
          borderWidth: 1,
          borderColor: 'black',
          marginTop: 20,
        }}
      />
    </View>
  );
};

export default ImagePickerDemo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonStyle: {
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20,
    width: 200,
    backgroundColor: 'green',
    marginBottom: 10,
  },
  buttonText: {
    fontSize: 18,
    color: '#fff',
    textAlign: 'center',
  },
});
