import React from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';

const MessagesScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Text style={{fontSize: 22, marginBottom: 30}}>In Progress</Text>
      {/* <Button
        title="Go Back"
        onPress={() => navigation.navigate('VehicleInfoScreen')}
      /> */}
    </View>
  );
};

export default MessagesScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#000',
  },
});
