import React from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';

const NotificationsScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Text style={{fontSize: 22, marginBottom: 30}}>In Progress</Text>
      {/* <Button
        title="Go Back"
        onPress={() => navigation.navigate('VehicleInfoScreen')}
      /> */}
    </View>
  );
};

export default NotificationsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#000',
  },
});
