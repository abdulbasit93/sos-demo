import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const VehicleInfoCard = () => {
  return (
    <View
      style={{
        marginLeft: 15,
        marginRight: 15,
        marginTop: 20,
      }}>
      <View
        style={{
          backgroundColor: '#252525',
          paddingTop: 20,
          paddingBottom: 20,
          borderTopLeftRadius: 10,
          borderTopRightRadius: 10,
        }}>
        <Text
          style={{
            color: '#00AEEE',
            fontSize: 25,
            fontWeight: '600',
            textAlign: 'center',
          }}>
          Vehicle Info
        </Text>
      </View>
      <View
        style={{
          backgroundColor: '#363636',
          padding: 15,
          borderBottomLeftRadius: 10,
          borderBottomRightRadius: 10,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text style={styles.vehicleInfoLeftText}>VIN No.</Text>
          <Text style={{marginTop: 5}}>5UXXW3C53J0T80683</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text style={styles.vehicleInfoLeftText}>Registration</Text>
          <Text style={{marginTop: 5}}>AL-5467</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text style={styles.vehicleInfoLeftText}>Type</Text>
          <Text style={{marginTop: 5}}>Sedan</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text style={styles.vehicleInfoLeftText}>Brand</Text>
          <Text style={{marginTop: 5}}>Audi</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text style={styles.vehicleInfoLeftText}>Model</Text>
          <Text style={{marginTop: 5}}>A4</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text style={{...styles.vehicleInfoLeftText, marginBottom: 10}}>
            Year
          </Text>
          <Text style={{marginTop: 5}}>2007</Text>
        </View>
      </View>
    </View>
  );
};

export default VehicleInfoCard;

const styles = StyleSheet.create({
  vehicleInfoLeftText: {
    fontSize: 20,
    color: '#3095BA',
    fontWeight: '400',
    marginBottom: 30,
  },
});
