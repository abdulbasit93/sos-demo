import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  ImageBackground,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome5';
import Modal from 'react-native-modal';
import {NavigationContainer} from '@react-navigation/native';

import {DrawerItems, DrawerActions} from '@react-navigation/drawer';

import {useNavigation} from '@react-navigation/core';

const {width: screenWidth} = Dimensions.get('screen');
// screen width Multiply by 0.8
const DATA = [
  {
    id: 'id-1',
    title: 'My Profile',
    Icon: () => <Icon style={{fontSize: 20, marginRight: 30}} name="user" />,
  },
  {
    id: 'id-2',
    title: 'My Vehicles',
    Icon: () => <Icon style={{fontSize: 20, marginRight: 30}} name="car" />,
  },
  {
    id: 'id-3',
    title: 'Service Requests',
    Icon: () => (
      <Icon style={{fontSize: 20, marginRight: 30}} name="clipboard-list" />
    ),
  },
  {
    id: 'id-4',
    title: 'Notifications',
    Icon: () => <Icon style={{fontSize: 20, marginRight: 30}} name="bell" />,
  },
  {
    id: 'id-5',
    title: 'Wallet',
    Icon: () => <Icon style={{fontSize: 20, marginRight: 30}} name="wallet" />,
  },
  {
    id: 'id-6',
    title: 'Messages',
    Icon: () => (
      <Icon style={{fontSize: 20, marginRight: 30}} name="comments" />
    ),
  },
  {
    id: 'id-7',
    title: 'Active Services',
    Icon: () => <Icon style={{fontSize: 20, marginRight: 30}} name="cogs" />,
  },
  {
    id: 'id-8',
    title: 'Contact us',
    Icon: () => (
      <Icon style={{fontSize: 20, marginRight: 30}} name="envelope" />
    ),
  },
  {
    id: 'id-9',
    title: 'Privacy Policy',
    Icon: () => (
      <Icon style={{fontSize: 20, marginRight: 30}} name="file-alt" />
    ),
  },
  {
    id: 'id-10',
    title: <Text style={{color: '#28C5F3'}}>Sign Out</Text>,
    Icon: () => (
      <Icon style={{fontSize: 20, marginRight: 30}} name="sign-out-alt" />
    ),
  },
];

const renderItem = ({item: {title, Icon}}) => (
  <View style={styles.item}>
    <Icon />
    <Text style={styles.title}>{title}</Text>
  </View>
);

const Header = () => {
  const [isModalVisible, setModalVisible] = useState(false);
  const [isRemoveVehicleModalVisible, setRemoveVehicleModalVisible] =
    useState(false);

  const toggleModal = () => {
    setModalVisible(prevState => !prevState);
  };

  const toggleRemoveVehicleModal = () => {
    setRemoveVehicleModalVisible(prevState => !prevState);
  };

  const navigation = useNavigation();

  return (
    <View>
      <ImageBackground
        source={require('../assets/images/car-bg.jpg')}
        style={styles.image}
        resizeMode="cover">
        <View style={styles.navButtons}>
          <TouchableOpacity onPress={() => alert('Back button pressed!')}>
            <Icon name="chevron-left" size={23} color="#00AEEE" />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.openDrawer()}>
            <Icon name="bars" size={23} color="#00AEEE" />
          </TouchableOpacity>
        </View>
        <View style={{marginBottom: 10, marginVertical: 110}}>
          <Text style={styles.headingText}>Audi A4</Text>
        </View>
        <View style={styles.btnContainer}>
          <TouchableOpacity style={styles.buttonStyle}>
            <Icon name="pen" size={13} color="#fff" style={{marginTop: 3}} />
            <Text style={(styles.btnText, {marginLeft: 9})}>Edit</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.buttonStyle}>
            <Icon
              name="share-alt"
              size={13}
              color="#fff"
              style={{marginTop: 3}}
            />
            <Text style={(styles.btnText, {marginLeft: 9})}>Share</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.buttonStyle}>
            <Text style={styles.btnText}>Export</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.buttonStyle}
            onPress={toggleRemoveVehicleModal}>
            <Icon name="trash" size={13} color="#fff" style={{marginTop: 3}} />
            <Text style={(styles.btnText, {marginLeft: 9})}>Remove</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
      <Modal
        isVisible={isModalVisible}
        animationIn="slideInRight"
        animationOut="slideOutRight"
        animationInTiming={500}
        animationOutTiming={500}
        hasBackdrop={true}
        backdropColor="#252525"
        backdropOpacity={0.8}
        backdropTransitionInTiming={500}
        backdropTransitionOutTiming={500}
        onBackdropPress={() => setModalVisible(false)}
        style={{alignItems: 'flex-end', margin: 0}}>
        <View
          style={{
            backgroundColor: '#252525',
            flex: 1,
            paddingTop: 50,
            width: screenWidth * 0.7,
          }}>
          <View style={styles.userContainer}>
            <View>
              <Image
                style={{width: 40, height: 40}}
                source={require('../assets/images/avatar.png')}
              />
            </View>

            <View>
              <Text style={{fontSize: 18}}>Richard Doe</Text>
              <Image
                style={{width: 90, height: 18}}
                source={require('../assets/images/five-stars.png')}
              />
            </View>
          </View>

          <View style={styles.container}>
            <FlatList
              data={DATA}
              renderItem={renderItem}
              keyExtractor={item => item.id}
            />
          </View>
        </View>
      </Modal>

      {/* Remove Vehicle Modal */}
      <Modal
        isVisible={isRemoveVehicleModalVisible}
        hasBackdrop={true}
        backdropColor="#252525"
        backdropOpacity={0.8}
        backdropTransitionInTiming={500}
        backdropTransitionOutTiming={500}
        animationIn="zoomIn"
        animationOut="zoomOut">
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            paddingTop: 60,
            paddingBottom: 60,
            backgroundColor: '#363636',
            borderRadius: 15,
          }}>
          <View
            style={{
              backgroundColor: '#00AEF0',
              padding: 34,
              borderRadius: 85,
              position: 'absolute',
              bottom: 230,
            }}>
            <Icon name="trash-alt" size={40} color="#000000" />
          </View>
          <Text
            style={{
              fontSize: 22,
              fontWeight: 'bold',
              color: '#08ABE9',
              marginTop: 20,
              marginBottom: 12,
            }}>
            Delete Vehicle
          </Text>
          <Text
            style={{
              textAlign: 'center',
              fontSize: 17,
              lineHeight: 26,
              padding: 10,
            }}>
            Are you sure you want to delete Vehicle "Audi A4"?. This will
            permanently remove your account.
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              position: 'absolute',
              top: 250,
            }}>
            <TouchableOpacity
              onPress={() => setRemoveVehicleModalVisible(false)}
              style={{
                backgroundColor: '#252525',
                paddingLeft: 35,
                paddingRight: 35,
                paddingTop: 15,
                paddingBottom: 15,
                borderWidth: 1,
                borderColor: '#3D3D3D',
                borderRadius: 30,
                marginRight: 25,
              }}>
              <Text style={{fontSize: 18, fontWeight: '700', color: '#00AEEF'}}>
                Cancel
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                backgroundColor: '#252525',
                paddingLeft: 35,
                paddingRight: 35,
                paddingTop: 15,
                paddingBottom: 15,
                borderWidth: 1,
                borderColor: '#3D3D3D',
                borderRadius: 30,
                marginLeft: 25,
              }}>
              <Text style={{fontSize: 18, fontWeight: '700', color: '#00AEEF'}}>
                Delete
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  navButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
  },
  image: {
    width: '100%',
    height: 280,
  },
  btnContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  buttonStyle: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    backgroundColor: '#1CB4EC',
    paddingTop: 9,
    paddingBottom: 9,
    paddingLeft: 17,
    paddingRight: 17,
    borderRadius: 50,
  },
  headingText: {
    color: '#00AEEE',
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  item: {
    padding: 10,
    marginVertical: 10,
    marginHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#313131',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  title: {
    fontSize: 14,
    justifyContent: 'center',
    alignItems: 'center',
  },
  userContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
});
