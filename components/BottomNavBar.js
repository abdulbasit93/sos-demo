import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome5';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import HomeIcon from 'react-native-vector-icons/Fontisto';

import {useNavigation} from '@react-navigation/native';

const BottomNavBar = () => {
  const navigation = useNavigation();
  return (
    <View
      style={{
        backgroundColor: '#252525',
        paddingTop: 20,
        paddingBottom: 25,
        marginTop: 30,
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25,
        width: Dimensions.get('screen').width,
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          width: Dimensions.get('screen').width,
        }}>
        <TouchableOpacity
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            marginLeft: 20,
          }}>
          <Icon name="comments" size={28} color="#fff" />
          <Text style={{fontSize: 11, marginTop: 10}}>Messages</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            marginLeft: 15,
          }}>
          <Icon name="wallet" size={28} color="#fff" />
          <Text style={{fontSize: 11, marginTop: 10}}>Wallet</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('HomeScreen')}
          style={{
            backgroundColor: '#00AEF0',
            padding: 20,
            borderRadius: 40,
            marginLeft: 20,
            marginRight: 20,
            position: 'absolute',
            top: -57,
            left: 136,
          }}>
          <HomeIcon name="home" size={33} color="#fff" />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            marginLeft: 73,
          }}>
          <FontAwesomeIcon name="gears" size={28} color="#fff" />
          <Text style={{fontSize: 11, marginTop: 10}}>Active Services</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            // marginLeft: 15,
            marginRight: 15,
          }}>
          <Icon name="bell" size={28} color="#fff" />
          <Text style={{fontSize: 11, marginTop: 10}}>Notifications</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default BottomNavBar;

const styles = StyleSheet.create({});
