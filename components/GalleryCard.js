import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';

import Modal from 'react-native-modal';
import * as ImagePicker from 'react-native-image-picker';

import UploadIcon from 'react-native-vector-icons/AntDesign';

import {useNavigation} from '@react-navigation/core';

import placeholderImage from '../assets/images/image-placeholder-icon-5.jpg';

const GalleryCard = () => {
  const navigation = useNavigation();
  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const [isModalVisible, setModalVisible] = useState(false);
  const [filePath, setFilePath] = useState({data: '', uri: ''});
  const [fileData, setFileData] = useState('');
  const [fileUri, setFileUri] = useState('');

  const launchCamera = () => {
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchCamera(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        const source = {
          uri: response.assets[0].uri,
        };
        console.log('source --> ', response.assets[0].uri);
        setFilePath(response);
        setFileData(response.data);
        setFileUri(source);
      }
    });
    toggleModal();
  };

  const launchImageLibrary = () => {
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchImageLibrary(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        const source = {uri: response.assets[0].uri};
        console.log('response', JSON.stringify(response));
        setFilePath(response);
        setFileData(response.data);
        setFileUri(response.uri);
        console.log('source --> ', source);
        setFilePath(response);
        setFileData(response.data);
        setFileUri(source);
      }
    });
    toggleModal();
  };

  return (
    <View style={styles.gallerySection}>
      <Modal
        isVisible={isModalVisible}
        onBackdropPress={toggleModal}
        onBackButtonPress={toggleModal}>
        <View style={{backgroundColor: '#8132FF', padding: 70}}>
          <Text style={{textAlign: 'center', fontSize: 22, marginBottom: 30}}>
            Upload an Image
          </Text>
          <TouchableOpacity
            style={{backgroundColor: '#00AEEE', padding: 18, width: 200}}
            onPress={() => launchCamera()}>
            <Text>Take Image from Camera</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: '#00AEEE',
              padding: 18,
              width: 200,
              marginTop: 10,
            }}
            onPress={() => launchImageLibrary()}>
            <Text>Pick from Image Gallery</Text>
          </TouchableOpacity>
        </View>
      </Modal>
      <View style={styles.galleryInner}>
        <Text style={{fontSize: 25, fontWeight: '600', color: '#00AEEE'}}>
          Gallery
        </Text>
        <TouchableOpacity
          onPress={toggleModal}
          style={{
            backgroundColor: '#00AEF0',
            width: 40,
            padding: 10,
            borderRadius: 50,
            marginLeft: 90,
          }}>
          <UploadIcon name="upload" size={20} color="#fff" />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('GalleryScreen', {key: fileUri})}
          style={{
            backgroundColor: '#1CB4EC',
            paddingTop: 10,
            paddingBottom: 10,
            width: '30%',
            borderRadius: 50,
          }}>
          <Text style={styles.btnText}>View All</Text>
        </TouchableOpacity>
      </View>

      <ScrollView
        horizontal={true}
        style={{
          backgroundColor: '#363636',
          padding: 20,
          borderBottomLeftRadius: 10,
          borderBottomRightRadius: 10,
        }}>
        <Image
          source={fileUri ? fileUri : placeholderImage}
          style={{width: 140, height: 150, borderRadius: 5, marginRight: 15}}
        />
        <Image
          source={require('../assets/images/601502.jpg')}
          style={{width: 140, height: 150, borderRadius: 5, marginRight: 15}}
        />
        <Image
          source={require('../assets/images/601502.jpg')}
          style={{width: 140, height: 150, borderRadius: 5, marginRight: 15}}
        />
        <Image
          source={require('../assets/images/601502.jpg')}
          style={{
            width: 140,
            height: 150,
            borderRadius: 5,
            marginRight: 42,
          }}
        />
      </ScrollView>
    </View>
  );
};

export default GalleryCard;

const styles = StyleSheet.create({
  gallerySection: {
    padding: 15,
  },
  galleryInner: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#252525',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 20,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  btnText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 14,
  },
});
