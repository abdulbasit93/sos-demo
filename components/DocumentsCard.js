import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';

import Modal from 'react-native-modal';

import DocumentPicker from 'react-native-document-picker';
import FileViewer from 'react-native-file-viewer';

import UploadIcon from 'react-native-vector-icons/AntDesign';

import {useNavigation} from '@react-navigation/core';

import documentPlaceholder from '../assets/images/dummy_PDF_doc.png';

const DocumentsCard = () => {
  const navigation = useNavigation();
  const [singleFile, setSingleFile] = useState('');
  const [singleFileName, setSingleFileName] = useState('');
  const [documentModal, setDocumentModal] = useState(false);

  const selectOneFile = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });

      console.log('res : ' + JSON.stringify(res));
      const fileUri = {uri: res[0].uri};
      const fileName = res[0].name;
      console.log('file name is -->', fileName);
      console.log('Uri --> ' + fileUri);
      setSingleFile(fileUri);
      setSingleFileName(fileName);

      const path = fileUri.uri;

      FileViewer.open(path)
        .then(() => {
          // success
          console.log('Document ' + path + ' successfully opened!');
        })
        .catch(error => {
          // error
          alert(error.message);
        });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        alert('Canceled from single doc picker');
      } else {
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };

  const toggleDocumentUploadModal = () => {
    setDocumentModal(!documentModal);
  };

  return (
    <View style={styles.gallerySection}>
      <Modal
        isVisible={documentModal}
        onBackdropPress={toggleDocumentUploadModal}
        onBackButtonPress={toggleDocumentUploadModal}>
        <View style={{backgroundColor: 'blue', padding: 70}}>
          <Text style={{textAlign: 'center', fontSize: 22, marginBottom: 30}}>
            Upload a Document
          </Text>
          <TouchableOpacity
            style={{backgroundColor: '#00AEEE', padding: 18, width: 200}}
            onPress={() => selectOneFile()}>
            <Text style={{textAlign: 'center'}}>Select File</Text>
          </TouchableOpacity>
        </View>
      </Modal>
      <View
        style={{
          ...styles.galleryInner,
          borderTopLeftRadius: 10,
          borderTopRightRadius: 10,
        }}>
        <Text style={{fontSize: 25, fontWeight: '600', color: '#00AEEE'}}>
          Documents
        </Text>
        <TouchableOpacity
          onPress={() => toggleDocumentUploadModal()}
          style={{
            backgroundColor: '#00AEF0',
            width: 40,
            padding: 10,
            borderRadius: 50,
            marginLeft: 38,
          }}>
          <UploadIcon name="upload" size={20} color="#fff" />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('DocumentsScreen', {key: singleFileName})
          }
          style={{
            backgroundColor: '#1CB4EC',
            paddingTop: 10,
            paddingBottom: 10,
            width: '30%',
            borderRadius: 50,
          }}>
          <Text style={styles.btnText}>View All</Text>
        </TouchableOpacity>
      </View>
      <ScrollView
        horizontal={true}
        style={{
          backgroundColor: '#363636',
          padding: 20,
          borderBottomLeftRadius: 10,
          borderBottomRightRadius: 10,
        }}>
        {singleFile.uri ? (
          <View
            style={{
              backgroundColor: '#AB140C',
              width: 140,
              height: 150,
              borderRadius: 5,
              marginRight: 15,
              padding: 10,
            }}>
            <Text style={{textAlign: 'center'}}>
              Document Uploaded: {singleFileName}
            </Text>
          </View>
        ) : (
          <Image
            source={require('../assets/images/dummy_PDF_doc.png')}
            style={{
              width: 140,
              height: 150,
              borderRadius: 5,
              marginRight: 15,
            }}
          />
        )}
        <Image
          source={require('../assets/images/dummy_XL_doc.png')}
          style={{width: 140, height: 150, borderRadius: 5, marginRight: 15}}
        />
        <Image
          source={require('../assets/images/dummy_PDF_doc.png')}
          style={{width: 140, height: 150, borderRadius: 5, marginRight: 15}}
        />
        <Image
          source={require('../assets/images/dummy_XL_doc.png')}
          style={{width: 140, height: 150, borderRadius: 5, marginRight: 42}}
        />
      </ScrollView>
    </View>
  );
};

export default DocumentsCard;

const styles = StyleSheet.create({
  gallerySection: {
    padding: 15,
  },
  galleryInner: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#252525',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 20,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  btnText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 14,
  },
});
